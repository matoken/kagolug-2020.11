#!/bin/bash

SOURCE='Twitterをちっちゃいリソースで.asciidoc'
BASE=`basename "$SOURCE" .asciidoc`
HTML=$BASE.html
PDF=$BASE.pdf

#echo "$SOURCE"
#echo "$HTML"
#echo "$PDF"
#exit

banner "adoc2html"
echo asciidoctor-revealjs-linux ./"$SOURCE"
asciidoctor-revealjs-linux ./"$SOURCE"
echo
banner "html2pdf"
echo chromium --user-data-dir=`mktemp -d` --headless --print-to-pdf=./"$PDF" file://`pwd`/"$HTML"?print-pdf
chromium --user-data-dir=`mktemp -d` --headless --print-to-pdf=./"$PDF" file://`pwd`/"$HTML"?print-pdf
#echo
#banner "pdf diet"
#echo mv "$PDF" "$BASE-org.pdf"
#mv "$PDF" "$BASE-org.pdf"
#echo ps2pdf "$BASE-org.pdf" $BASE-diet.pdf
#ps2pdf "$BASE-org.pdf" $BASE-diet.pdf
banner sign
#echo mv "$PDF" "$BASE-org.pdf"
#mv "$PDF" "$BASE-org.pdf"
echo gpg -u 5071E226B689C97F1481579CCB0449E0CF1A89BF --clearsign "$PDF"
gpg -u 5071E226B689C97F1481579CCB0449E0CF1A89BF -b -a "$PDF"
#echo rm "$BASE-org.pdf"
#rm "$BASE-org.pdf"
echo gpg --verify $PDF.asc
gpg --verify $PDF.asc
